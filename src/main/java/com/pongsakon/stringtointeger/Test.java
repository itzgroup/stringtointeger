/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.stringtointeger;

import java.util.Scanner;

/**
 *
 * @author 66955
 */
public class Test {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        long start, stop;
        start = System.nanoTime();
        String word = kb.next();
        System.out.println(word + " is of type " + ((Object) word).getClass().getSimpleName());
        int num = Integer.parseInt(word);
        System.out.println(num + " is of type " + ((Object) num).getClass().getSimpleName());
        stop = System.nanoTime();
        System.err.println("time = " + (stop - start) * 1E-9 + "secs.");
    }
}
